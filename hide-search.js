(function() {
    var css = document.getElementById('hide-search');
    if (!css) {
        css = document.createElement('style');
        css.id = "hide-search";
        css.appendChild(document.createTextNode(
            ".hide-search .p-client { grid-template-rows: 0 auto min-content !important; }"
            + ".hide-search .p-top_nav { visibility: hidden; }"
            + ".hide-search.p-client_desktop--ia-top-nav .c-fullscreen_modal { top: 0 !important; height: 100% !important; }"
        ));
        document.getElementsByTagName("head")[0].appendChild(css);
    }

    var body = document.querySelector("body");
    if (body.className.indexOf('hide-search') !== -1) {
        body.className = body.className.replace(' hide-search', '');
    } else {
        body.className += ' hide-search';
    }
})();
